<section class="top-destinations">
    <div class="container">
        <div class="section-title text-center">
            <h2>Top Destinations</h2>
            <div class="section-icon">
                <i class="flaticon-diamond"></i>
            </div>
            <p>Most beautiful places to see in Sri Lanka</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-xs-4">
                @foreach($featuredPackages as $key => $featuredPackage)
                    @if ($key ==0 || $key ==1)
                        <a href="{{route('page.single',['package'=>$featuredPackage->package_id])}}">
                            <div class="top-destination-item">
                                <img class="img-responsive img-1"
                                     src="images/packages/{{$featuredPackage->package->image}}"
                                     alt="Image">
                                <div class="overlay">
                                    <h2>
                                        <a href="{{route('page.single',['package'=>$featuredPackage->package_id])}}">{{$featuredPackage->package->name}}</a>
                                    </h2>
                                    <p>Plan Your Tour With Us.</p>
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
            <div class="col-md-4 col-xs-4">
                @foreach($featuredPackages as $key => $featuredPackage)
                    @if ($key == 2)
                        <a href="{{route('page.single',['package'=>$featuredPackage->package_id])}}">
                            <div class="top-destination-item destination-margin">
                                <img class="img-responsive img-2"
                                     src="images/packages/{{$featuredPackage->package->image}}"
                                     alt="Image">
                                <div class="overlay overlay-full">
                                    <h2>
                                        <a href="{{route('page.single',['package'=>$featuredPackage->package_id])}}">{{$featuredPackage->package->name}}</a>
                                    </h2>
                                    <p>Plan Your Tour With Us.</p>
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach
            </div>
            <div class="col-md-4 col-xs-4">
                @foreach($featuredPackages as $key => $featuredPackage)
                    @if ($key ==3 || $key ==4)
                        <a href="{{route('page.single',['package'=>$featuredPackage->package_id])}}">
                            <div class="top-destination-item">
                                <img class="img-responsive img-3"
                                     src="images/packages/{{$featuredPackage->package->image}}"
                                     alt="Image">
                                <div class="overlay">
                                    <h2>
                                        <a href="{{route('page.single',['package'=>$featuredPackage->package_id])}}">{{$featuredPackage->package->name}}</a>
                                    </h2>
                                    <p>Plan Your Tour With Us.</p>
                                </div>
                            </div>
                        </a>
                    @endif
                @endforeach

            </div>
        </div>
    </div>
</section>
<style>
    .img-1 {
        height: 270px !important;
    }

    .img-2 {
        height: 545px !important;
    }

    .img-3 {
        height: 270px !important;
    }
</style>
