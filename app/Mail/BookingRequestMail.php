<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $bookTour;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bookTour)
    {
        $this->bookTour=$bookTour;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('srilankatourgo@gmail.com', 'Sri Lanka Tour Go')
            ->subject('New Booking Request Submitted')->markdown('emails.booking-request');
    }
}
