<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\PackageImage;
use App\Models\PackageTimeline;
use App\Models\PackageDescription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package = Package::orderBy('id', 'DESC')->paginate(5);
        return view('admin.package.index', compact('package'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.package.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required',
            'meet_point' => 'required',
            'meet_date' => 'required',
            'meet_time' => 'required',
            'departure_point' => 'required',
            'departure_date' => 'required',
            'departure_time' => 'required',
            'min_person' => 'required',
            'max_person' => 'required',
            'languages' => 'required',
            'includes' => 'required',
            'excludes' => 'required',
            'popular_places' => 'required',
            'other' => 'required',
            'map_data' => 'required',
            'distance' => 'required',
            'duration' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect(route('packages.create'))
                ->withErrors($validator)
                ->withInput();
        }

        // $filename = null;
        // if ($request->hasfile('image')) {
        //     $file = $request->file('image');
        //     $extension = $file->getClientOriginalExtension();
        //     $filename = time() . '.' . $extension;
        //     $file->move('images/packages/', $filename);
        // }
        
         foreach ($request->photos as $photo) {

            $destinationPath = 'images/packages/';
            $filename = $photo->getClientOriginalName();
            $photo->move($destinationPath, $filename);
            $package = new Package();
            $package->file_name = $filename;
            $package->package_id = $package->id;
            $package->save();
        }
        DB::transaction(function () use ($request, $filename) {
            $package = Package::create(['name' => $request->name, 'description' => $request->description, 'image' => $filename]);
            $package->packageDescription()->create($request->except(['name', 'description', 'image']));
        });



        return redirect(route('packages.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::findOrFail($id);

     return view('admin.package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Package $package
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Package $package)
    {
        $package->delete();
        return  redirect(route('packages.index'));
    }

    public function singlepackage(Request $request, Package $package)
    {
        return view('admin.packagetimeline.index', compact('package'));
    }

    public function packageTimeLineAddForm(Package $package){
        return view('admin.packagetimeline.create',compact('package'));
    }


    public function images(Request $request,Package $package){
        return view('admin.package.package-images',compact('package'));
    }

    public function imageForm(Request $request,Package $package){
        return view('admin.package.package-images-form',compact('package'));
    }

    public function saveImage(Request $request,Package $package){
        $filename = null;
        $filePath='images/packages/'.$package->id.'/';
        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move($filePath, $filename);
        }
        $image=new PackageImage();
        $image->file_path=$filePath;
        $image->file_name=$filename;
        $image->order=$request->order;
        $image->package()->associate($package);
        $image->save();
        return redirect(route('package.images',$package->id));
    }

}
