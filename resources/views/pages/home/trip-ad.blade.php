<!-- Trip Ad -->
<section class="trip-ad">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-xs-12">

                <div class="trip-ad-content">
                    <div class="ad-title">
                        <h2>Explore The <span>Nuwara Eliya</span></h2>
                    </div>
                    <p> If visiting Sri Lanka don't miss the tea plantations in higher elevation. To watch the tea pickers harvesting the leaves you need to get up early. They start working with cool and pleasant temps at sunrise before it is getting too hot for them to work.</p>
                    <p> Nuwara Eliya has a mild climate without a real "dry" season. From December to February/March nights are cold and even frost can occur, but it warms up during the day. The driest months are February and March. Heavy precipitation occurs from October to December.</p>
                    <div class="trip-ad-btn">
                        <a href="{{route('page.single',['package'=>'1'])}}" class="btn-blue btn-red">BOOK NOW</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="ad-price">
                    <div class="ad-price-inner">
                        <span>Days <span class="rate">09</span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Trip Ad Ends -->
