<!-- Testimonials -->
<section class="testimonials">
    <div class="section-title text-center">
        <h2>Happy Clients</h2>
        <div class="section-icon section-icon-white">
            <i class="flaticon-diamond"></i>
        </div>
    </div>
    <!-- Paradise Slider -->
    <div id="testimonial_094" class="carousel slide testimonial_094_indicators thumb_scroll_x swipe_x ps_easeOutSine"
         data-ride="carousel" data-pause="hover" data-interval="3000" data-duration="1000">


        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#testimonial_094" data-slide-to="0" class="active">
                <img src="images/testemonial1.jpg" alt="testimonial_094_01"> <!-- 1st Image -->
            </li>
            <li data-target="#testimonial_094" data-slide-to="1">
                <img src="images/testemonial2.jpg" alt="testimonial_094_02"> <!-- 2nd Image -->
            </li>
            <li data-target="#testimonial_094" data-slide-to="2">
                <img src="images/testemonial3.jpg" alt="testimonial_094_03"> <!-- 3rd Image -->
            </li>
        </ol>

        <!-- Wrapper For Slides -->
        <div class="carousel-inner" role="listbox">

            <!-- First Slide -->
            <div class="item active">
                <!-- Text Layer -->
                <div class="testimonial_094_slide">
                    <p>Our trip to Sri Lanka was one of the best we made in
                        the last few years. After friends of ours told us about
                        their holiday to Sri Lanka two years ago, they
                        recommended us their tour guide.When it was
                        clear for us where we wanted to spend our holiday this
                        year, we got into contact with Sri Lanka Toour Go via email, which
                        worked splendidly even though we mailed him only two
                        weeks before our departure! (Advice: Do it at least a
                        month before your departure to Sri Lanka to ensure
                        your trip will go smoothly.) However, he was able to
                        manage and plan our round tour just according to our
                        wishes.
                    </p>
                    <div class="deal-rating">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star-o"></span>
                    </div>
                    {{--                    <h5><a href="#">Susan Doe, Houston</a></h5>--}}
                </div> <!-- /Text Layer -->
            </div> <!-- /item -->
            <!-- End of First Slide -->

            <!-- Second Slide -->
            <div class="item">
                <!-- Text Layer -->
                <div class="testimonial_094_slide">
                    <p> My boyfriend and I wished for a quick 7 day tour of Sri
                        Lanka and Sri Lanka Tour GO was very responsive in offering us a
                        quote which targeted everything we wanted to see.
                        Our tour griver CM was waiting for us at the airport
                        on arrival. Absolutely
                        fantastic experience of Sri Lanka, learnt about their
                        traditions, culture and history and saw some lovely
                        landscapes and went on a great Safari. CM was a
                        great tour guide and driver. Thank you CM! Would
                        highly recommend CM to anyone wanting to travel
                        Sri Lanka!</p>
                    <div class="deal-rating">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                    </div>
                    {{--                    <h5><a href="#">Susan Doe, Houston</a></h5>--}}
                </div> <!-- /Text Layer -->
            </div> <!-- /item -->
            <!-- End of Second Slide -->

            <!-- Third Slide -->
            <div class="item">
                <!-- Text Layer -->
                <div class="testimonial_094_slide">
                    <p>It all started with a very intensive planning time and
                        lots of eMails with Sri Lanka Tour Go thanks again for your great
                        tips and very quick responses! In Sri Lanka CM
                        picked us up at the airport and really did his very best
                        to comply all our wishes. CM was not only our driver
                        but also our guide, if possible sometimes you need
                        certificates - and we really felt like a team! Every
                        evening we discussed the next day and the best
                        options due to weather, our special wishes and latest
                        information from other guides.</p>
                    <div class="deal-rating">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star-o"></span>
                    </div>
                    {{--                    <h5><a href="#">Susan Doe, Houston</a></h5>--}}
                </div> <!-- /Text Layer -->
            </div> <!-- /item -->
            <!-- End of Third Slide -->

        </div> <!-- End of Wrapper For Slides -->
    </div> <!-- End Paradise Slider -->
</section>
<!-- Testimonials -->
