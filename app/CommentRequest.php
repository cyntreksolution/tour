<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentRequest extends Model
{
     protected $fillable=['comment','name','email','website_link'];

     public function posts(){
    	return $this->hasMany('App\Post');
    }
}
