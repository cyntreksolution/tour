<div class="comment-form">
    <form role="form" action="{{route('comments.store')}}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="package_id" value="{{$package->id}}">
        @csrf
        <h3>Add a comment</h3>
        <div class="row">
            <div class="form-group col-sm-12">
                <label for="Name">Your Comment:</label>
                <textarea type="text" name="comment" required></textarea>
            </div>
            <div class="form-group col-sm-6">
                <label for="Name">Name:</label>
                <input type="text" name="name" class="form-control" required id="Name">
            </div>
            <div class="form-group col-sm-6">
                <label for="email">Email address:</label>
                <input type="email" name="email" class="form-control" required id="email">
            </div>
            <div class="col-sm-12">
                <div class="comment-btn">
                    <button class="btn-blue btn-red">Submit Comment</button>
                </div>
            </div>
        </div>
    </form>
</div>
