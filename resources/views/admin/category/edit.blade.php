@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <!-- Listings -->
                    <div class="col-lg-12 col-sm-12">
                        <div id="add-listing">

                            <!-- Section -->
                            <div class="add-listing-section">

                                <!-- Headline -->
                                <div class="add-listing-headline">
                                    <h3><i class="sl sl-icon-doc"></i> Category Add</h3>
                                </div>

                                <!-- Title -->
                                <form role="form" action="{{ route('category.update',$category->id) }}" method="post" enctype="multipart/form-data">
                                 @csrf
                                @method('PUT')

                                <!-- Row -->
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Category Name <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="name" id="" type="text" value="{{$category->name}}"/>
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Icon <i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="text" name="icon" id="icon" value="{{$category->icon}}" placeholder="">
                                    </div>

                                </div>
                                 <button type="submit" class="btn btn-primary">Submit</button>
                                
                                <!-- Row / End -->
                            </form>
                            </div>
                            <!-- Section / End -->
                        </div>
                    </div>
                </div>
</div>      
@endsection      