@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="dashboard-list-box">
                            <h4 class="gray">All Comments</h4>
                            <div class="table-box">
                            <table class="basic-table booking-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Package</th>
                                        <th>Writer</th>
                                        <th>Comment</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($comments as $key => $comment)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td><h4>{{$comment->package->name}}</h4></td>
                                        <td><h4>{{$comment->name}}</h4></td>
                                        <td><h4>{{mb_strimwidth($comment->comment,0,20,'...')}}</h4></td>
                                        <td>
                                            <a href="{{route('show.comment',['comment'=>$comment->id])}}" class="btn btn-success">show</a>
                                        @if ($comment->status==0)
                                           <a href="{{route('approve.comment',['comment'=>$comment->id])}}" class="btn btn-primary">approve</a>
                                            @else
                                           <a href="{{route('disapprove.comment',[$comment->id])}}" class="btn btn-danger">disapprove</a>
                                        @endif
                                        @if ($comment->status==1)
                                           <a href="{{route('reply.comment',['comment'=>$comment->id])}}" class="btn btn-primary">reply</a>
                                            @else
                                           <a href="{{route('reply.comment',['comment'=>$comment->id])}}" class="btn btn-primary disabled">reply</a>
                                        @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){


        $(".comment-container").delegate(".reply","click",function(){

            var well = $(this).parent().parent();
            var cid = $(this).attr("cid");
            var name = $(this).attr('name_a');
            var token = $(this).attr('token');
            var form = '<form method="post" action="/replies"><input type="hidden" name="_token" value="'+token+'"><input type="hidden" name="comment_id" value="'+ cid +'"><input type="hidden" name="name" value="'+name+'"><div class="form-group"><textarea class="form-control" name="reply" placeholder="Enter your reply" > </textarea> </div> <div class="form-group"> <input class="btn btn-primary" type="submit"> </div></form>';

            well.find(".reply-form").append(form);



        });

        $(".comment-container").delegate(".delete-comment","click",function(){

            var cdid = $(this).attr("comment-did");
            var token = $(this).attr("token");
            var well = $(this).parent().parent();
            $.ajax({
                    url : "/comments/"+cdid,
                    method : "POST",
                    data : {_method : "delete", _token: token},
                    success:function(response){
                    if (response == 1 || response == 2) {
                        well.hide();
                    }else{
                        alert('Oh ! you can delete only your comment');
                        console.log(response);
                    }
                }
            });

        });

        $(".comment-container").delegate(".reply-to-reply","click",function(){
            var well = $(this).parent().parent();
            var cid = $(this).attr("rid");
            var rname = $(this).attr("rname");
            var token = $(this).attr("token")
            var form = '<form method="post" action="/replies"><input type="hidden" name="_token" value="'+token+'"><input type="hidden" name="comment_id" value="'+ cid +'"><input type="hidden" name="name" value="'+rname+'"><div class="form-group"><textarea class="form-control" name="reply" placeholder="Enter your reply" > </textarea> </div> <div class="form-group"> <input class="btn btn-primary" type="submit"> </div></form>';

            well.find(".reply-to-reply-form").append(form);

        });


    });
            </script>
@endsection
