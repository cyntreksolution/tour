@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <!-- Listings -->
                    <div class="col-lg-12 col-sm-12">
                        <div id="add-listing">
                            <form role="form" action="{{ route('package-timelines.store',['package'=>$package->id]) }}" method="post" enctype="multipart/form-data">
                                 @csrf
                            <!-- Section -->
                            <div class="add-listing-section">

                                <!-- Headline -->
                                <div class="add-listing-headline">
                                    <h3><i class="sl sl-icon-doc"></i> Timeline Add</h3>
                                </div>

                                <!-- Row -->
                                <div class="row with-forms">


                                      <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Order<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input class="search-field" name="order" type="text" value=""/ required="required">
                                    </div>

                                    <div class="col-md-6">
                                        <label>Title<i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="title" id="" type="text" value="" required="required" />
                                    </div>

                                </div>

                                 <div class="form">
                                    <label>Description</label>
                                    <textarea class="WYSIWYG" name="description" cols="40" rows="3" id="summary" spellcheck="true"></textarea>
                                </div>

                                <!-- Row / End -->
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <!-- Section / End -->

                                 

                                <!-- Row / End -->

                            </div>
                            <!-- Section / End -->
                            </form>
                        </div>
                    </div>
                </div>
</div>
@endsection
