<?php
/**
 * Model object generated by: Skipper (http://www.skipper18.com)
 * Do not modify this file manually.
 */

namespace App\Models\AbstractModels;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractCategory extends Model
{

    /**
     * Primary key type.
     *
     * @var string
     */
    protected $keyType = 'bigInteger';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'icon' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime'
    ];

    public function packages()
    {
        return $this->belongsToMany('\App\Models\Package', 'category_packages', 'category_id', 'package_id')->withPivot('id', 'created_at', 'updated_at');
    }
}
