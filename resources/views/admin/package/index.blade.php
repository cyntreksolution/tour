@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="dashboard-list-box">
                            <a href="{{url('packages/create')}}" class="button preview">Add Packages<i class="fa fa-arrow-circle-right"></i></a>
                            <h4 class="gray">All Packages</h4>
                            <div class="table-box">
                            <table class="basic-table booking-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Timeline Add</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package as $key => $data)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->description}}</td>
                                        <td>
                                            <a href="{{route('timeline.single',['package'=>$data->id])}}" class="label label-success" style="padding: 10px;">Add</a>
                                        </td>
                                        <td>
                                            <a href="{{route('package.images',[$data->id])}}" class="button gray"><i class="sl sl-icon-puzzle"></i></a>
                                            <a href="{{route('packages.edit',[$data->id])}}" class="button gray"><i class="sl sl-icon-pencil"></i></a>
                                            <form action="{{ route('packages.destroy', $data->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"  type="submit"><i class="sl sl-icon-close"></i></button>
                                            <!-- <a href="#" class="button gray"><i class="sl sl-icon-close"></i></a> -->
                                            </form>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        {{ $package->links() }}
                    </div>
                </div>
            </div>
@endsection
