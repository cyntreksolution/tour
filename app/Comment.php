<?php

namespace App;

use App\Models\Package;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=['package_id','name','email','comment'];

    public function replyComment(){
        return $this->hasOne(Comment::class,'parent_id','id');
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }

    public function scopeApprovedComments($query){
        return $query->whereStatus(1)->notReplyComment();
    }

    public function scopeNotReplyComment($query){
       return $query->whereNull('parent_id');
    }



}
