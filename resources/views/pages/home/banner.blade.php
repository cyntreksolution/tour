<section id="home_banner">
    <!-- Paradise Slider -->
    <div id="kenburns_061" class="carousel slide ps_indicators_txt_icon ps_control_txt_icon kbrns_zoomInOut thumb_scroll_x swipe_x ps_easeOutQuart" data-ride="carousel" data-pause="hover" data-interval="10000" data-duration="2000">
        <!-- Wrapper For Slides -->
        <div class="carousel-inner" role="listbox">

            <!-- First Slide -->
            <div class="item active">
                <!-- Slide Background -->
                <img src="images/slider/slider1.jpeg" alt="kenburns_061_01" />
                <!-- Left Slide Text Layer -->
                <div class="kenburns_061_slide" data-animation="animated fadeInRight">
                    <h2>Get a Quote & Just Leave</h2>
                    <h1>Find your next trip in Sri Lanka</h1>

                    <a href="{{route('page.all')}}" class="btn-blue btn-red">View Our Tour</a>
                </div><!-- /Left Slide Text Layer -->
            </div><!-- /item -->
            <!-- End of Slide -->

            <!-- Second Slide -->
            <div class="item">
                <!-- Slide Background -->
                <img src="images/slider/slider2.jpeg" alt="kenburns_061_02" />
                <!-- Right Slide Text Layer -->
                <div class="kenburns_061_slide kenburns_061_slide_right" data-animation="animated fadeInUp">
                    <h2>exciting schemes just a click away</h2>
                    <h1>Amazing Ella 9 days tour</h1>
                    <a href="{{route('page.single',['package'=>3])}}" class="btn-blue btn-red">View Our Tour</a>
                </div><!-- /Right Slide Text Layer -->
            </div><!-- /item -->

            <!-- Third Slide -->
            <div class="item">
                <!-- Slide Background -->
                <img src="images/slider/slider3.jpeg" alt="kenburns_061_02" />
                <!-- Right Slide Text Layer -->
                <div class="kenburns_061_slide kenburns_061_slide_center" data-animation="animated fadeInDown">
                    <h2>Cost friendly packages on your way</h2>
                    <h1>We offer you better deals</h1>
                    <a href="{{route('page.all')}}" class="btn-blue btn-red">View Our Tour</a>
                </div><!-- /Right Slide Text Layer -->
            </div><!-- /item -->
            <!-- End of Slide -->

        </div>

    </div>
</section>
