

<!-- Countdown -->
<section class="countdown-section">
    <div class="container">
        <div class="countdown-title">
            <h2>Special Tour in 20th November 2019, Discover <span>Ella</span> for 20 Customers with <span>Discount 30%</span></h2>
            <p>It’s limited seating! Hurry up</p>
        </div>
        <div class="countdown countdown-container container">
            <p id="demo"></p>
        </div><!-- /.countdown-wrapper -->
    </div>
    <div class="testimonial-overlay"></div>
</section>
<!-- Countdown Ends -->
