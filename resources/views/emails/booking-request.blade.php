@component('mail::message')
# Hi {{$bookTour->name}}

Your booking  Request for {{$bookTour->package->name }} on {{$bookTour->booking_date}} has been submitted.You will be contact as soon as possible.

This is an automated email. Please do not reply to this email.<br>
Thanks,<br>
{{ preg_replace('/([a-z])([A-Z])/s','$1 $2', config('app.name')) }}
@endcomponent
