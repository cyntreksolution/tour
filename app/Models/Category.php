<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends \App\Models\AbstractModels\AbstractCategory
{
    use SoftDeletes;
    protected $fillable = ['name', 'slug', 'icon'];
}
