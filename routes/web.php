<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('sitemap.xml', function (){
   return view('sitemap');
})->name('page.sitemap');
Route::get('/', 'PageController@index')->name('page.home');
Route::get('package/{package}', 'PageController@showPackage')->name('page.single');
Route::get('all/package', 'PageController@showAllPackage')->name('page.all');
Route::get('category/{category}/packges', 'PageController@showAllPackage')->name('page.category');
Route::post('packages/search', 'PageController@showAllPackage')->name('packages.search');
Route::get('about', 'PageController@about')->name('page.about');
Route::get('categories', 'PageController@categories')->name('page.categories');
Route::get('contact', 'PageController@contact')->name('page.contact');
Route::get('blog', 'PageController@blog')->name('page.blog');


Route::post('booking/request', 'PageController@bookTourRequest')->name('booking.request');
Route::post('contact/request', 'PageController@contactUsRequest')->name('contact.request');

Route::get('admin', 'DashboardController@admin')->name('dashboard.admin');
//Route::get('debug', 'DashboardController@debug')->name('dashboard.debug');
//
//Route::resource('category', 'CategoryController');
//Route::resource('packages', 'PackageController');
////
//Route::resource('category', 'CategoryController');
//Route::resource('packages', 'PackageController');
//Route::resource('package-timelines', 'PackageTimelineController');
//
//Route::get('package/{package}/timeline', 'PackageController@singlepackage')->name('timeline.single');
//Route::get('package/{package}/timeline/form', 'PackageController@packageTimeLineAddForm')->name('timeline.form');
////
//Route::get('package/{package}/images', 'PackageController@images')->name('package.images');
//Route::get('package/{package}/images-form', 'PackageController@imageForm')->name('package.imagesform');
//Route::post('package/{package}/images-form', 'PackageController@saveImage')->name('package.imagesform');
//
Route::resource('comments', 'CommentController');

Auth::routes();

Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/comments/all', 'DashboardController@comments')->name('all.comments');
    Route::get('/comment/{comment}/show', 'DashboardController@showComment')->name('show.comment');
    Route::get('/comment/{comment}/approve', 'DashboardController@approveComment')->name('approve.comment');
    Route::get('/comment/{comment}/disapprove', 'DashboardController@disapproveComment')->name('disapprove.comment');
    Route::get('/comment/{comment}/reply', 'DashboardController@reply')->name('reply.comment');
    Route::post('/comment/{comment}/reply-save', 'DashboardController@saveReply')->name('reply.save');
});
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


