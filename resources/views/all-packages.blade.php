@extends('layouts.default')

@section('content')
    <section class="breadcrumb-outer text-center">
        <div class="container">
            <div class="breadcrumb-content">
                <h2>All Packages</h2>
            </div>
        </div>
        <div class="section-overlay"></div>
    </section>
    <!-- BreadCrumb Ends -->

    <!-- Listing -->
    <section class="blog-list grid-list">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="blog-wrapper">
                        <div class="row">
                            @foreach($packages as $package)
                                <div class="col-sm-6 col-xs-12">
                                    <div class="blog-item">
                                        <div class="blog-image">
                                            <img class="img-pck" src="{{asset('images/packages/'.$package->image)}}" alt="Image">
                                        </div>
                                        <div class="blog-content">
                                            <h3>
                                                <a href="{{route('page.single',['package'=>$package->id])}}">{{$package->name}}</a>
                                            </h3>
                                            <p> {{$package->packageDescription->other}} </p>

                                            <div class="blog-author">
                                                <div class="pull-left">
                                                    <p><i class="flaticon-time"></i> {{$package->packageDescription->duration}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    @if($packages instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        <div class="col-sm-12">
                            <div class="pagination-content">
                                {{ $packages->links() }}
                            </div>
                        </div>
                    @endif

                </div>


                <div id="sidebar-sticky" class="col-md-4 col-sm-12">
                    <aside class="detail-sidebar sidebar-wrapper">
                        <div class="item-sidebar">
                            <div class="sidebar-item sidebar-item-dark">
                                <div class="detail-title">
                                    <h3>Search</h3>
                                </div>
                                <form action="{{route('packages.search')}}" method="post">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-xs-12">
                                            <input type="text" class="form-control" name="search"
                                                   placeholder="Enter the place you want to search">
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="comment-btn">
                                                <button type="submit" class="btn-blue btn-red">Search Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="post-categories sidebar-item">
                                <div class="detail-title">
                                    <h3>Categories</h3>
                                </div>
                                <ul>
                                    @foreach($categories as $category)
                                        <li>
                                            <a href="{{route('page.category',['category'=>$category->id])}}">{{$category->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="popular-post sidebar-item">
                                <div class="detail-title">
                                    <h3>Popular Trips</h3>
                                </div>
                                @foreach($popularPackages as $key => $popularPackage)
                                    <div class="popular-item">
                                        <div class="popular-content">
                                            <span class="item-no">{{++$key}}</span>
                                            <h4>
                                                <a href="{{route('page.single',['package'=>$popularPackage->package_id])}}">{{$popularPackage->package->name}}</a>
                                            </h4>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
    <style>
        .img-pck{
            height: 240px !important;
        }
    </style>
@stop
