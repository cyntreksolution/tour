@extends('layouts.default')

@section('content')
    <section class="breadcrumb-outer text-center">
        <div class="container">
            <div class="breadcrumb-content">
                <h2>About Us</h2>
            </div>
        </div>
        <div class="section-overlay"></div>
    </section>

    <!-- About Us -->
    <section class="aboutus">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title text-center">
                        <h2>Why Choose Us?</h2>
                        <div class="section-icon section-icon-white">
                            <i class="flaticon-diamond"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="about-item">
                        <div class="about-icon">
                            <i class="fa fa-superpowers" aria-hidden="true"></i>
                        </div>
                        <div class="about-content">
                            <h3>Perfect Planning</h3>
                            <p>Plan your perfect vacation with our travel agency. Choose among hundreds of all-inclusive offers!</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="about-item">
                        <div class="about-icon">
                            <i class="fa fa-paw" aria-hidden="true"></i>
                        </div>
                        <div class="about-content">
                            <h3>Secure Venues</h3>
                            <p>We work hard to secure the best hotel rates in the most popular destinations. Search and book adventure tours now!</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="about-item">
                        <div class="about-icon">
                            <i class="fa fa-fire" aria-hidden="true"></i>
                        </div>
                        <div class="about-content">
                            <h3>Beautiful Memories</h3>
                            <p>Book international vacation packages with us and create memories that will last a lifetime! Create History !</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Us Ends -->

    @include('pages.home.testimonials')

    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div id="contact-form" class="contact-form">

                        <div id="contactform-error-msg"></div>

                        <form method="post" action="{{route('contact.request')}}">
                            @csrf
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <label>Name:</label>
                                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter full name" required>
                                </div>
                                <div class="form-group col-xs-6">
                                    <label>Email:</label>
                                    <input type="email" name="email" class="form-control" id="email" placeholder="abc@xyz.com" required>
                                </div>
                                <div class="form-group col-xs-6 col-left-padding">
                                    <label>Phone Number:</label>
                                    <input type="text" name="telephone" class="form-control" id="telephone" placeholder="XXXX-XXXXXX" required>
                                </div>
                                <div class="textarea col-xs-12">
                                    <label>Message:</label>
                                    <textarea name="message" placeholder="Enter a message" required></textarea>
                                </div>
                                <div class="col-xs-12">
                                    <div class="comment-btn">
                                        <input type="submit" class="btn-blue btn-red"  value="Send Message">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-about footer-margin">
                        <div class="about-logo">
                           <h3> Sri Lanka Tour Go</h3>
{{--                            <img src="images/Yatra-01.png" alt="Image">--}}
                        </div>
                        <h4>Travel With Us</h4>
                        <p>The Best Travel Agent in Sri lanka to give most worth to your holiday in Sri Lanka</p>
                        <div class="contact-location">
                            <ul>
                                <li><i class="flaticon-maps-and-flags" aria-hidden="true"></i> {{$agent->address}}</li>
                                <li><i class="flaticon-phone-call"></i> <a
                                        href="tel:{{$agent->telephone}}">{{$agent->telephone}}</a></li>
                                <li><i class="flaticon-mail"></i>  <a href="mailto:{{$agent->email}}">{{$agent->email}}</a></li>
                            </ul>
                        </div>
                        <div class="footer-social-links">
                            <ul>
                                @if($agent->facebook)
                                    <li class="social-icon"><a target="_blank" href="{{$agent->facebook}}"><i class="fa fa-facebook"
                                                                                              aria-hidden="true"></i></a>
                                    </li>
                                @endif
                                @if($agent->insta)
                                    <li class="social-icon"><a target="_blank" href="{{$agent->insta}}"><i class="fa fa-instagram"
                                                                                           aria-hidden="true"></i></a>
                                    </li>
                                @endif
                                @if($agent->twitter)
                                    <li class="social-icon"><a target="_blank" href="{{$agent->twitter}}"><i class="fa fa-twitter"
                                                                                             aria-hidden="true"></i></a>
                                    </li>
                                @endif
                                @if($agent->youtube)
                                    <li class="social-icon"><a target="_blank" target="_blank" href="{{$agent->youtube}}">
                                            <i class="fa fa-youtube" aria-hidden="true"></i></a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
