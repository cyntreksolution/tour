<!--* bucket*-->
<section id="bucket-list" class="bucket-list">

    <div class="bucket-icons">
        <div class="container">
            <div class="section-title text-center">
                <h2>Top up your Bucket List</h2>
                <div class="section-icon">
                    <i class="flaticon-diamond"></i>
                </div>
                <p> We care of everything according to your wishes, highly flexible to your entire satisfaction.More Categories More Packages </p>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="mt_filter">
                        <ul class="list-inline text-center filter">
                            @foreach($categories as $category)
                                <li><a href="#" data-filter=".{{$category->slug}}">
                                        <i class="{{$category->icon}}"></i>
                                        <span>  {{$category->name}}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bucket-content">
        <div class="container">
            <div class="row isotopeContainer">
                @foreach($packages as $package)
                    <div
                        class="col-sm-6 col-xs-12 no-padding isotopeSelector {{str_replace(['"','[',"]",",","\t"],["","",""," ",""],$package->category_list)}}">
                        <div class="hovereffect-bucket bucket-item">
                            <div class="bucket-image"><img src="images/packages/{{$package->image}}" alt="image"
                                                           class="img-responsive"/>
                            </div>
                            <div class="bucket-item-content">
                                <h3>
                                    <a href="{{route('page.single',['package'=>$package->id])}}">{{$package->name}}</a>
                                </h3>
                                <span>{{(!empty($package->packageDescription->duration))?$package->packageDescription->duration:null}} </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="section-overlay"></div>
        </div>
    </div>
</section>
<style>
    .bucket-image img {
        width: 130px !important;
        height: 90px !important;
    }
</style>
<!--* End bucket*-->
