@component('mail::message')
# Hi

Your have new contact request. <br> <br>

Name: {{$contact->name}} <br>
Email: {{$contact->email}} <br>
Telephone: {{$contact->telephone}} <br>
Message: {{$contact->message}} <br>


This is an automated email. Please do not reply to this email.<br>
Thanks,<br>
{{ preg_replace('/([a-z])([A-Z])/s','$1 $2', config('app.name')) }}
@endcomponent
