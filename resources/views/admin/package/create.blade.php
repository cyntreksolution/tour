@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <!-- Listings -->
                    <div class="col-lg-12 col-sm-12">
                        <div id="add-listing">
                            <form role="form" action="{{ action('PackageController@store') }}" method="post" enctype="multipart/form-data">
                                 @csrf
                            <!-- Section -->
                            <div class="add-listing-section">

                                <!-- Headline -->
                                <div class="add-listing-headline">
                                    <h3><i class="sl sl-icon-doc"></i> Package Add</h3>
                                </div>

                                <!-- Title -->
                                

                                <!-- Row -->
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Package name <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="name" id="" type="text" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Image<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <!-- <input type="file" name="image" id="image" placeholder="" required="required"> -->
                                         <br>
                                          Product photos (can attach more than one): <br>
                                         <input multiple="multiple" name="photos[]" type="file"> 
                          
                                         
                                    </div>

                                </div>

                                 <div class="form">
                                    <label>Description</label>
                                    <textarea class="WYSIWYG" name="description" cols="40" rows="3" id="summary" spellcheck="true"></textarea>
                                </div>
                                
                                <!-- Row / End -->
                            
                            </div>
                            <!-- Section / End -->

                            <div class="add-listing-section">

                                <!-- Headline -->
                                <div class="add-listing-headline">
                                    <h3><i class="sl sl-icon-doc"></i> Package Description Add</h3>
                                </div>

                                <!-- Title -->
                                <div class="row with-forms">
                                    <div class="col-md-12">
                                        <label>Meet point <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="meet_point" type="text" value=""/>
                                    </div>
                                </div>

                                <!-- Row -->
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Meet date <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="meet_date" id="" type="date" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Meet time<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="time" name="meet_time" id="icon" placeholder="" required="required">
                                    </div>

                                </div>

                                <div class="row with-forms">
                                    <div class="col-md-12">
                                        <label>Departure point <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="departure_point" type="text" value=""/>
                                    </div>
                                </div>

                                <!-- Row -->
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Departure date <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="departure_date" id="" type="date" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Departure time<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="time" name="departure_time" id="" placeholder="" required="required">
                                    </div>

                                </div>

                                 <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Min person <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="min_person" id="" type="text" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Max person <i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="text" name="max_person" id="" placeholder="" required="required">
                                    </div>

                                </div>
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Languages <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="languages" id="" type="text" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Includes<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="text" name="includes" id="" placeholder="" required="required">
                                    </div>

                                </div>
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Excludes <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="excludes" id="" type="text" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Popular places<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="text" name="popular_places" id="" placeholder="" required="required">
                                    </div>

                                </div>
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Other <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="other" id="" type="text" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Map data<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="text" name="map_data" id="" placeholder="" required="required">
                                    </div>

                                </div>
                                <div class="row with-forms">

                                    
                                    <div class="col-md-6">
                                        <label>Distance <i class="tip" data-tip-content="Name of your business"></i></label>
                                        <input class="search-field" name="distance" id="" type="text" value="" required="required" />
                                    </div>
                                    

                                    <!-- Type -->
                                    <div class="col-md-6">
                                        <label>Duration<i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></label>
                                        <input type="text" name="duration" id="" placeholder="" required="required">
                                    </div>

                                </div>

                                 <button type="submit" class="btn btn-primary">Submit</button>
                                
                                <!-- Row / End -->
                            
                            </div>
                            <!-- Section / End -->
                            </form>
                        </div>
                    </div>
                </div>
</div>      
@endsection      