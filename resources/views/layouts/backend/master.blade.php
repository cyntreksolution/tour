<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zxx">


<!-- Mirrored from cyclonethemes.com/demo/html/yatra/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2017], Sun, 06 Oct 2019 08:03:13 GMT -->
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard | SL Tour Go</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo1.png')}}">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <!--Custom CSS-->
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <!--Flaticons CSS-->
    <link href="{{asset('font/flaticon.css')}}" rel="stylesheet" type="text/css">
    <!--Plugin CSS-->
    <link href="{{asset('css/plugin.css')}}" rel="stylesheet" type="text/css">
    <!--Dashboard CSS-->
    <link href="{{asset('css/dashboard.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/icons.css')}}" rel="stylesheet" type="text/css">
    <!--Font Awesome-->
    <link rel="stylesheet" href="{{asset('cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css')}}">

</head>
<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- Preloader Ends -->

    <!-- start Container Wrapper -->
    <div id="container-wrapper">
        <!-- Dashboard -->
        <div id="dashboard">

            <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger"><i class="fa fa-reorder"></i> Dashboard Navigation</a>

            <div class="dashboard-sticky-nav">
                <div class="content-left pull-left">
                    <a href="dashboard.html">
                        <h2 style="color: white">Sri Lanka Tour Go</h2>
                    </a>
                </div>
                <div class="content-right pull-right">
                    <div class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <div class="profile-sec">
                                <div class="dash-image">
                                    <img src="{{asset('images/comment.jpg')}}" alt="">
                                </div>
                                <div class="dash-content">
                                    <h4>{{(!empty(Auth::user()->name))?Auth::user()->name:'Guest User'}}</h4>
                                    <span>Manager</span>
                                </div>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
{{--                            <li><a href="#"><i class="sl sl-icon-lock"></i>Change Password</a></li>--}}
                            <li><a href="/logout"><i class="sl sl-icon-power"></i>Logout</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">
                    <ul>
                        <li class="active"><a href="{{route('all.comments')}}"><i class="sl sl-icon-settings"></i> All Comments</a></li>
{{--                        <li class="active"><a href="{{url('admin')}}"><i class="sl sl-icon-settings"></i> Dashboard</a></li>--}}
{{--                        <li><a href="{{url('category')}}"><i class="sl sl-icon-plus"></i> Category</a></li>--}}
{{--                        <li><a href="{{url('packages')}}"><i class="sl sl-icon-plus"></i> Packages</a></li>--}}
                        <!-- <li>
                            <a><i class="sl sl-icon-layers"></i> Tour Listing</a>
                            <ul>
                                <li><a href="dashboard-list.html">Active <span class="nav-tag green">6</span></a></li>
                                <li><a href="dashboard-list.html">Pending <span class="nav-tag yellow">1</span></a></li>
                                <li><a href="dashboard-list.html">Expired <span class="nav-tag red">2</span></a></li>
                            </ul>
                        </li>
                        <li><a href="dashboard-booking.html"><i class="sl sl-icon-list"></i> Booking List</a></li>
                        <li><a href="dashboard-history.html"><i class="sl sl-icon-folder"></i> History</a></li>
                        <li><a href="dashboard-reviews.html"><i class="sl sl-icon-star"></i> Reviews</a></li>
                        <li><a href="index-2.html"><i class="sl sl-icon-power"></i> Logout</a></li> -->
                    </ul>
                </div>
            </div>

            @yield('content')

             <!-- Copyrights -->
            <div class="copyrights">
                <p>2018 <i class="fas fa-copyright"></i> powered by <a href="www.cyntrek.com" target="_blank">Cyntrek Solutions</a></p>
            </div>
        </div>
        <!-- Dashboard / End -->
    </div>
    <!-- end Container Wrapper -->


    <!-- Back to top start -->
    <div id="back-to-top">
        <a href="#"></a>
    </div>
    <!-- Back to top ends -->

    <!-- *Scripts* -->
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/plugin.js')}}"></script>
    <script src="{{asset('js/preloader.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('canvasjs.com/assets/script/canvasjs.min.js')}}"></script>
    <script src="{{asset('js/chart.js')}}"></script>
    <script src="{{asset('js/dashboard-custom.js')}}"></script>
    <script src="{{asset('js/jpanelmenu.min.js')}}"></script>
    <script src="{{asset('js/counterup.min.js')}}"></script>
</body>

<!-- Mirrored from cyclonethemes.com/demo/html/yatra/dashboard.html by HTTrack Website Copier/3.x [XR&CO'2017], Sun, 06 Oct 2019 08:03:21 GMT -->
</html>
