<!-- Deals On Sale -->
<section class="deals-on-sale">
    <div class="container">
        <div class="section-title text-center">
            <h2>Deals On Sale</h2>
            <div class="section-icon">
                <i class="flaticon-diamond"></i>
            </div>
            <p>Perfectly organized Sri Lankan Tours. Enjoy relaxed days full of amazing insights in our rich Sinhalese culture, our green landscape, amazing wildlife and beautiful beaches.</p>
        </div>
        <div class="row sale-slider slider-button">
            @foreach($dealPackages as $dealPackage)
                <div class="col-md-12">
                    <div class="sale-item">
                        <div class="sale-image">
                            <img src="images/packages/{{$dealPackage->package->image}}" alt="Image">
                        </div>
                        <div class="sale-content">
                            <div class="sale-review">
                                @php
                                    $rating= $dealPackage->package->rating;
                                @endphp
                                @for($i=0;$i<5;$i++)
                                    @if($i<$rating)
                                        <span class="fa fa-star checked"></span>
                                    @else
                                        <span class="fa fa-star-o"></span>
                                    @endif
                                @endfor
                            </div>
                            <h3><a href="#">{{$dealPackage->package->name}}</a></h3>
                            <p><i class="flaticon-time"></i> {{$dealPackage->package->packageDescription->duration}}</p>
                            <p>{{$dealPackage->package->packageDescription->other}}</p>
                            <a href="{{route('page.single',['package'=>$dealPackage->package_id])}}" class="btn-blue btn-red">View More</a>
                        </div>

                        <div class="sale-overlay"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<style>
    .sale-item img{
        height: 366px !important;
    }
</style>
<!-- Deals On Sale Ends -->
