<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;



class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $category = Category::orderBy('id','DESC')->paginate(5);
        return view('admin.category.index',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        

         $validator = Validator::make($request->all(), [
            'name' => 'required',
            'icon' => 'required',
            
            
        ]);

        if ($validator->fails()) {
            return redirect(route('category.create'))
                ->withErrors($validator)
                ->withInput();
        }

        $names = $request->name;
        $lowcase_name = strtolower($names);
        $slug = str_replace(" ","-",$lowcase_name);
        $data = array(
        'name' => $request->name,
        'slug' => $slug,
        'icon' => $request->icon,
        );
        // $category=Category::create($request->all());
        DB::table('categories')->insert($data);
        
        return redirect(route('category.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $validator = Validator::make($request->all(), [
            'name' => 'required',
             'icon' => 'required',

        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput()
                ->withErrors($validator)
                ->withInput();
        }

        $names = $request->name;
        $lowcase_name = strtolower($names);
        $slug = str_replace(" ","-",$lowcase_name);

        $categories= category::find($id);
        $categories->name=$request->name;
        $categories->slug=$slug;
        $categories->icon=$request->icon;


        $categories->save();
        return redirect(route('category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        
        return redirect('/category')->with('success', 'Show is successfully deleted');


    }
}
