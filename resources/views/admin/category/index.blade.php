@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="dashboard-list-box">
                            <a href="{{url('category/create')}}" class="button preview">Add Category<i class="fa fa-arrow-circle-right"></i></a>
                            <h4 class="gray">All Category</h4>
                            <div class="table-box">
                            <table class="basic-table booking-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Icon</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($category as $key => $data)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$data->name}}</td>
                                        <td>{{$data->slug}}</td>
                                        <td><i class="{{$data->icon}}"></i></td>
                                        <td>
                                            <a href="{{route('category.edit',[$data->id])}}" class="button gray"><i class="sl sl-icon-pencil"></i></a>
                                            <form action="{{ route('category.destroy', $data->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit"><i class="sl sl-icon-close"></i></button>
                                            <!-- <a href="#" class="button gray"><i class="sl sl-icon-close"></i></a> -->   
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        {{ $category->links() }}
                    </div>
                </div>
            </div>
@endsection