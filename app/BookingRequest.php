<?php

namespace App;

use App\Models\Package;
use Illuminate\Database\Eloquent\Model;

class BookingRequest extends Model
{
   protected $fillable=[
       'package_id',
       'name',
       'email',
       'telephone',
       'telephone2',
       'booking_date',
       'message',
       'message',
   ];

   public function package(){
       return $this->belongsTo(Package::class);
   }
}
