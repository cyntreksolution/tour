@extends('layouts.backend.master')
@section('content')
    <div class="dashboard-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 traffic">
                <div class="dashboard-list-box">
                    <h4 class="gray">Recent Comments</h4>
                    <div class="table-box">
                        <table class="basic-table">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Booking ID</th>
                                <th>Destination</th>
                                <th>No of Tickets</th>
                                <th>Price</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>01/01/2017</td>
                                <td class="t-id">C001</td>
                                <td>Dubai</td>
                                <td>5</td>
                                <td>$300.00</td>
                            </tr>
                            <tr>
                                <td>01/01/2017</td>
                                <td class="t-id">C081</td>
                                <td>Grece - Zakynthos</td>
                                <td>5</td>
                                <td>$300.00</td>
                            </tr>
                            <tr>
                                <td>01/01/2017</td>
                                <td class="t-id">C001</td>
                                <td>Bulgary - Sunny Beach</td>
                                <td>5</td>
                                <td>$300.00</td>
                            </tr>
                            <tr>
                                <td>01/01/2017</td>
                                <td class="t-id">C001</td>
                                <td>France - Paris</td>
                                <td>5</td>
                                <td>$300.00</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
