@extends('layouts.backend.master')
@section('content')
    <div class="dashboard-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="dashboard-list-box">
                    <a href="{{route('package.imagesform',['package'=>$package->id])}}" class="button preview">Add Images<i class="fa fa-arrow-circle-right"></i></a>
                    <h4 class="gray">{{$package->name}}</h4>
                    <div class="table-box">
                        <table class="basic-table booking-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Order</th>
                                <th>Image</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($package->packageImages as $key => $image)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$image->order}}</td>
                                    <td><img class="img-x" src="{{asset($image->file_path.$image->file_name)}}"></td>
                                    <td>
{{--                                        <a href="{{route('package-timelines.edit',[$data->id])}}" class="button gray"><i class="sl sl-icon-pencil"></i></a>--}}
{{--                                        <form action="{{ route('package-timelines.destroy', $data->id)}}" method="post">--}}
{{--                                            @csrf--}}
{{--                                            @method('DELETE')--}}
{{--                                            <button class="btn btn-danger"  type="submit"><i class="sl sl-icon-close"></i></button>--}}
{{--                                            <!-- <a href="#" class="button gray"><i class="sl sl-icon-close"></i></a> -->--}}
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <style>
        .img-x {
            width: 100px !important;
        }
    </style>
@endsection
