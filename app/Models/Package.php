<?php

namespace App\Models;

use App\Comment;

class Package extends \App\Models\AbstractModels\AbstractPackage
{
    public function getCategoryListAttribute()
    {
        $category_list = collect();
        foreach ($this->categories as $category) {
            $category_list->push($category->slug);
        }
        return $category_list;
    }

    protected $appends = ["category_list"];

    protected $fillables = [
        'name','image','description',
    ];

    protected $guarded=['_token'];

    public function comments(){
        return $this->hasMany(Comment::class);
    }

}
