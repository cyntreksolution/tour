@extends('layouts.backend.master')
@section('content')
    <div class="dashboard-content">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="dashboard-list-box">
                    <h4 class="gray">Comment</h4>

                    <p> {{$comment->comment}}</p>


                    <h5 class="mt-5">add reply comment</h5>

                    <form role="form" action="{{ route('reply.save',$comment->id) }}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <textarea name="reply">{{(!empty($comment->replyComment->comment)?$comment->replyComment->comment:null)}}</textarea>


                        <div class="com-md-12">
                            @if ($comment->status==0)
                                <a href="{{route('approve.comment',['comment'=>$comment->id])}}"
                                   class="btn btn-primary">approve</a>
                            @else
                                <a href="{{route('disapprove.comment',[$comment->id])}}" class="btn btn-danger">disapprove</a>
                            @endif
                            <button type="submit" class="btn btn-primary">save reply</button>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
@endsection
