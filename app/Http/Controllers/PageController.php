<?php

namespace App\Http\Controllers;


use App\BookingRequest;
use App\CommentRequest;
use App\ContactRequest;
use App\Post;
use Illuminate\Support\Facades\DB;
use App\Mail\BookingRequestMail;
use Illuminate\Support\Facades\Validator;
use App\Mail\BookingRequestToAgentMail;
use App\Mail\ContactMail;
use App\Mail\ContactMailToAgent;
use App\Models\Agent;
use App\Models\Category;
use App\Models\Package;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class PageController extends Controller
{

    private $agent;
    private $popularPackages;
    private $featuredPackages;

    /**
     * PageController constructor.
     */
    public function __construct()
    {
        $this->agent = Agent::find(1);
        $this->popularPackages = $this->agent->popularPackages;
        $this->featuredPackages = $this->agent->featuredPackages;
        View::share('agent', $this->agent);
        View::share('popularPackages', $this->popularPackages);
        View::share('featuredPackages', $this->featuredPackages);
    }

    /**
     * @return View
     */
    public function index()
    {
        $dealPackages = $this->agent->dealsPackages;

        $categories = Category::all();
        $packages = Package::all();
        return view('welcome', compact('dealPackages', 'categories', 'packages'));
    }

    /**
     * @param Request $request
     * @param Package $package
     * @return View
     */
    public function showPackage(Request $request, Package $package)
    {
        $popularPackages = $this->agent->popularPackages;

        return view('package', compact('package', 'popularPackages'));
    }

    public function showAllPackage(Request $request)
    {
        $category = $request->category;
        $search = $request->search;
        $categories = Category::all();
        $popularPackages = $this->agent->popularPackages;
        $packages = Package::query();
        if ($category) {
            $packages = Category::whereId($category)->first()->packages;
        }
        if ($search) {
            $packages = Package::where('name', 'like', '%' . $search . '%')->get();
        }
        if (!$category && !$search) {
            $packages = $packages->paginate(8);
        }
        return view('all-packages', compact('categories', 'popularPackages', 'packages'));
    }

    public function about(Request $request)
    {
        return view('about');
    }

    public function categories(Request $request)
    {
        return view('categories');
    }

    public function contact(Request $request)
    {
        return view('contact');
    }

    public function blog()
    {
        $comments = CommentRequest::latest('created_at')->get();
        return view('blog', ['comments' => $comments]);
    }

    public function bookTourRequest(Request $request){
        $bookTour=BookingRequest::create($request->all());
        $agent=Agent::find(1);
        Mail::to($bookTour->email)->send(new BookingRequestMail($bookTour));
        Mail::to($agent->email)->send(new BookingRequestToAgentMail($bookTour));
        return redirect('/');
    }

    public function contactUsRequest(Request $request){
        $contact = ContactRequest::create($request->all());
        $agent=Agent::find(1);
        Mail::to($contact->email)->send(new ContactMail($contact));
        Mail::to($agent->email)->send(new ContactMailToAgent($contact));
        return redirect('/');
    }

    public function commentrequest(Request $request){
         $validator = Validator::make($request->all(), [
            'comment' => 'required',
            'name' => 'required',
            'email' => 'required',
            'website_link' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect(route('page.blog'))
                ->withErrors($validator)
                ->withInput();
        }


        $comment=CommentRequest::create($request->all());



        return redirect(route('page.blog'));
    }


}
