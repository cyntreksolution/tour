@extends('layouts.default')

@section('content')
    <section class="breadcrumb-outer text-center">
        <div class="container">
            <div class="breadcrumb-content">
                <h2>{{$package->name}}</h2>
            </div>
        </div>
        <div class="section-overlay"></div>
    </section>


    <section class="main-content detail">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-8">
                    <div class="detail-content content-wrapper">
                        <div class="detail-info">
                            <div class="detail-info-content clearfix">
                                <h2>{{$package->name}}</h2>
                                <div class="deal-rating">
                                    @php
                                        $rating= $package->rating;
                                    @endphp
                                    @for($i=0;$i<5;$i++)
                                        @if($i<$rating)
                                            <span class="fa fa-star checked"></span>
                                        @else
                                            <span class="fa fa-star-o"></span>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="gallery detail-box">
                            <!-- Paradise Slider -->
                            <div id="in_th_030"
                                 class="carousel slide in_th_brdr_img_030 thumb_scroll_x swipe_x ps_easeOutQuint"
                                 data-ride="carousel" data-pause="hover" data-interval="4000" data-duration="2000">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#in_th_030" data-slide-to="0" class="active">
                                        <img src="{{asset('images/packages/'.$package->image)}}" alt="in_th_030_01_sm"/>
                                    </li>
                                    @foreach($package->packageImages as $key => $packageImage)
                                        <li data-target="#in_th_030" data-slide-to="{{++$key}}">
                                            <img src="{{asset($packageImage->image)}}" alt="in_th_030_01_sm"/>
                                        </li>
                                    @endforeach
                                </ol>
                                <!-- /Indicators -->

                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="{{asset('images/packages/'.$package->image)}}" alt="in_th_030_01"/>
                                    </div>
                                    @foreach($package->packageImages as $key => $packageImage)
                                        <div class="item">
                                            <img src="{{asset($packageImage->image)}}" alt="in_th_030_02"/>
                                        </div>
                                    @endforeach
                                </div>

                            </div> <!-- End Paradise Slider -->
                        </div>


                        <div class="description detail-box">
                            <div class="detail-title">
                                <h3>Description</h3>
                            </div>
                            <div class="description-content">
                                <p>{{$package->description}}</p>
                                <table>
                                    <thead>
                                    </thead>
                                    <tbody>
                                    @if ($package->packageDescription->meet_point)
                                        <tr>
                                            <td class="title">Meet Point</td>
                                            <td>
                                                @foreach(explode(",",$package->packageDescription->meet_point) as $place)
                                                    <i class="fa fa-map-marker"
                                                       aria-hidden="true"></i> {{$place}}
                                            @endforeach
                                            <td>
                                        </tr>
                                    @endif

                                    @if ($package->packageDescription->meet_date)
                                        <tr>
                                            <td class="title">Meet Date</td>
                                            <td>
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                {{$package->packageDescription->meet_date}}
                                            </td>
                                        </tr>
                                    @endif

                                    @if ($package->packageDescription->meet_time)
                                        <tr>
                                            <td class="title">Meet Time</td>
                                            <td><i class="fa fa-clock-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->meet_time}}
                                            </td>
                                        </tr>
                                    @endif

                                    @if ($package->packageDescription->departure_point)
                                        <tr>
                                            <td class="title">Departure</td>
                                            <td>
                                                <i class="fa fa-map-marker"
                                                   aria-hidden="true"></i> {{$package->packageDescription->departure_point}}
                                            </td>
                                        </tr>
                                    @endif

                                    @if ($package->packageDescription->departure_date)
                                        <tr>
                                            <td class="title">Departure Date</td>
                                            <td><i class="fa fa-clock-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->departure_date}}
                                            </td>
                                        </tr>
                                    @endif

                                    @if ($package->packageDescription->departure_time)
                                        <tr>
                                            <td class="title">Departure Time</td>
                                            <td><i class="fa fa-clock-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->departure_time}}
                                            </td>
                                        </tr>
                                    @endif

                                    @if ($package->packageDescription->min_person)
                                        <tr>
                                            <td class="title">Minimun Travellers</td>
                                            <td><i class="fa fa-user-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->min_person}}
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($package->packageDescription->max_person)
                                        <tr>
                                            <td class="title">Maximum Travellers</td>
                                            <td><i class="fa fa-user-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->max_person}}
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($package->packageDescription->duration)
                                        <tr>
                                            <td class="title">Duration</td>
                                            <td>
                                                <i class="fa fa-clock-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->duration}}
                                            </td>
                                        </tr>
                                    @endif

                                    @if ($package->packageDescription->distance)
                                        <tr>
                                            <td class="title">Distance</td>
                                            <td>
                                                <i class="fa fa-clock-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->distance}}
                                            </td>
                                        </tr>
                                    @endif


                                    @if ($package->packageDescription->languages)
                                        <tr>
                                            <td class="title">Languages</td>
                                            <td>
                                                <i class="fa fa-file-audio-o"
                                                   aria-hidden="true"></i> {{$package->packageDescription->languages}}
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($package->packageDescription->includes)
                                        <tr>
                                            <td class="title">Includes</td>
                                            <td>
                                                <ul>
                                                    @foreach(explode(",",$package->packageDescription->includes) as $include)
                                                        <li>
                                                            <i class="fa fa-check" aria-hidden="true"></i> {{$include}}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($package->packageDescription->excludes)
                                        <tr>
                                            <td class="title">Excludes</td>
                                            <td class="excludes">
                                                <ul>
                                                    @foreach(explode(",",$package->packageDescription->excludes) as $exclude)
                                                        <li>
                                                            <i class="fa fa-times" aria-hidden="true"></i> {{$exclude}}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($package->packageDescription->popular_places)
                                        <tr>
                                            <td class="title">Popular Places</td>
                                            <td>
                                                <ul>
                                                    @foreach(explode(",",$package->packageDescription->popular_places) as $place)
                                                        <li>
                                                            <i class="fa fa-map-marker"
                                                               aria-hidden="true"></i> {{$place}}
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    @endif
                                    @if ($package->packageDescription->other)
                                        <tr>
                                            <td></td>
                                            <td>
                                                {{$package->packageDescription->other}}
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        @if ($package->packageDescription->map_data)
                            <div class="location-map detail-box">
                                <div class="detail-title">
                                    <h3>Location Map</h3>
                                </div>
                                <div class="map-frame">
                                    <iframe
                                        src="{{$package->packageDescription->map_data}}"
                                        style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>
                        @endif


                        @if (!empty($package->packageTimelines) && sizeof($package->packageTimelines)>0)
                            <div class="detail-timeline detail-box">
                                <div class="detail-title">
                                    <h3>Tour Timeline</h3>
                                </div>
                                <div class="timeline-content">
                                    <ul class="timeline">
                                        @foreach($package->packageTimelines as $day)
                                            <li>
                                                <div class="direction-r">
                                                    <div class="day-wrapper">
                                                        <span>{{$day->order}}</span>
                                                    </div>
                                                    <div class="flag-wrapper">
                                                        <span class="flag">{{$day->title}}</span>
                                                    </div>
                                                    <div class="desc">
                                                        <p>{{$day->description}}</p>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div id="sidebar-sticky" class="col-md-4">
                    <aside class="detail-sidebar sidebar-wrapper">
                        <div class="sidebar-item sidebar-item-dark">
                            <div class="detail-title">
                                <h3>Book this tour</h3>
                            </div>
                            <form action="{{route('booking.request')}}" method="post">
                                @csrf
                                <div class="row">
                                    <input type="hidden" name="package_id" value="{{$package->id}}">
                                    <div class="form-group col-xs-12">
                                        <input type="text" class="form-control" name="name" placeholder="Name" required>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <input type="email" class="form-control" name="email" placeholder="Email"
                                               required>
                                    </div>
                                    <div class="form-group col-xs-6 col-left-padding">
                                        <input type="number" class="form-control" name="telephone"
                                               placeholder="Phone Number" required>
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <input type="date" class="form-control" name="booking_date" required>
                                    </div>
                                    <div class="form-group col-xs-6 col-left-padding">
                                        <input type="number" placeholder="Phone Number 2" class="form-control"
                                               name="telephone2">
                                    </div>
                                    <div class="textarea col-xs-12">
                                        <textarea placeholder="Message" name="message" required></textarea>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="comment-btn">
                                            <button type="submit" class="btn-blue btn-red">Book Now</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="sidebar-item">
                            <div class="detail-title">
                                <h3>Popular Packages</h3>
                            </div>
                            <div class="sidebar-content sidebar-slider">
                                @foreach($popularPackages as $popularPackage)
                                    <div class="sidebar-package">
                                        <div class="sidebar-package-image">
                                            <img src="{{asset('images/packages/'.$popularPackage->package->image)}}"
                                                 alt="Images">
                                        </div>
                                        <div class="destination-content sidebar-package-content">
                                            <h4>
                                                <a href="{{route('page.single',['package'=>$popularPackage->package_id])}}">{{$popularPackage->package->name}}</a>
                                            </h4>
                                            <div class="deal-rating">
                                                @php
                                                    $rating= $popularPackage->package->rating;
                                                @endphp
                                                @for($i=0;$i<5;$i++)
                                                    @if($i<$rating)
                                                        <span class="fa fa-star checked"></span>
                                                    @else
                                                        <span class="fa fa-star-o"></span>
                                                    @endif
                                                @endfor
                                            </div>
                                            <p><i class="flaticon-time"></i> {{$popularPackage->package->name}} </p>


                                            <a href="#" class="btn-blue btn-red">Book Now</a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="sidebar-item sidebar-helpline">
                            <div class="sidebar-helpline-content">
                                <h3>Any Questions?</h3>
                                <p>The Best Travel Agent in Sri lanka to give most worth to your holiday in Sri Lanka
                                    is always ready for you. Do not hesitate to call or mail with us.</p>
                                <p><i class="flaticon-phone-call"></i> <a
                                        href="tel:{{$agent->telephone}}">{{$agent->telephone}}</a></p>
                                <p><i class="flaticon-mail"></i> <a
                                        href="mailto:{{$agent->email}}">{{$agent->email}}</a></p>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
            @if (!empty($package->comments()->approvedComments()->get()) && sizeof($package->comments()->approvedComments()->get())>0)
                <div class="comment-box">
                    <h3>Comments</h3>
                    <ul class="comment-list">
                        @foreach($package->comments()->approvedComments()->get() as $comment)
                            <li>
                                <div class="comment-item">
{{--                                    <div class="comment-image">--}}
{{--                                        <img src="{{asset('images/author.jpg')}}" alt="Image">--}}
{{--                                    </div>--}}
                                    <div class="comment-content">
                                        <h4>{{$comment->name}}</h4>
                                        <p class="date"><i
                                                class="fa fa-clock-o"></i>{{$comment->created_at->format('d/m/Y')}}</p>
                                        <p>{{$comment->comment}}</p>
                                    </div>
                                </div>
                                @if (!empty($comment->replyComment))
                                    <ul class="reply">
                                        <li>
                                            <div class="comment-item">
{{--                                                <div class="comment-image">--}}
{{--                                                    <img src="{{asset('images/author.jpg')}}" alt="Image">--}}
{{--                                                </div>--}}
                                                <div class="comment-content">
                                                    <h4>Administrator</h4>
                                                    <p>{{$comment->replyComment->comment}}</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @include('comment-form')

        </div>
    </section>
    <style>
        .sidebar-helpline-content a {
            color: white;
        }

        .sidebar-package-image img {
            width: 100%;
            height: 261px;
        }

        .carousel-indicators img {
            width: 96px;
            height: 96px;
        }

        .carousel-inner img {
            width: 616px !important;
            height: 369px !important;
        }
    </style>
@stop
