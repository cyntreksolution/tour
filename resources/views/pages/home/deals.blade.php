<!-- Deals -->
<section class="deals">
    <div class="container">
        <div class="section-title section-title-white text-center">
            <h2>Last Minute Deals</h2>
            <div class="section-icon">
                <i class="flaticon-diamond"></i>
            </div>
            <p>The Best deal on Sri Lanka to Travel around the Perl of Indian Ocean.Dont Miss it</p>
        </div>
        <div class="deals-outer">
            <div class="row deals-slider slider-button">
                @foreach($dealPackages as $dealPackage)
                    <div class="col-md-3">
                        <div class="deals-item">
                            <div class="deals-item-outer">
                                <div class="deals-image">
                                    <img src="images/packages/{{$dealPackage->package->image}}" alt="Image">
                                    <span class="deal-price">{{$dealPackage->package->packageDescription->duration}}</span>
                                </div>
                                <div class="deal-content">
                                    <div class="deal-rating">
                                        @php
                                            $rating= $dealPackage->package->rating;
                                        @endphp
                                        @for($i=0;$i<5;$i++)
                                            @if($i<$rating)
                                                <span class="fa fa-star checked"></span>
                                            @else
                                                <span class="fa fa-star-o"></span>
                                            @endif
                                        @endfor
                                    </div>
                                    <h3>{{$dealPackage->package->name}}</h3>
                                    <p>{{$dealPackage->package->packageDescription->other}}</p>
                                    <a href="{{route('page.single',['package'=>$dealPackage->package_id])}}" class="btn-blue btn-red">More Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="section-overlay"></div>
</section>
<style>
    .deals-slider img{
        height: 164px;
    }
</style>
<!-- Deals Ends -->
