<?php
namespace App\Models;

class PackageTimeline extends \App\Models\AbstractModels\AbstractPackageTimeline
{
	protected $fillables = [
        'order','title','description',
    ];
     protected $guarded=['_token'];
}
