<?php
namespace App\Models;

class PackageDescription extends \App\Models\AbstractModels\AbstractPackageDescription
{
	protected $fillables = [
        'package_id',
        'meet_point',
        'meet_date',
        'meet_time',
        'departure_point',
        'departure_date',
        'departure_time',
        'min_person',
        'max_person',
        'languages',
        'includes',
        'excludes',
        'popular_places',
        'other',
        'map_data',
        'distance',
        'duration',
    ];

    public function Package()
    {
      return $this->belongsTo('App\Package');
    }
    protected $guarded=['_token'];
}
