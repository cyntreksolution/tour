<section class="trusted-partners">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-4">
                <div class="partners-title">
                    <h3>Our <span>Partners</span></h3>
                </div>
            </div>
            <div class="col-md-9 col-xs-8">
                <ul class="partners-logo partners-slider">
                    <li><a href="#"><img src="images/partner1.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner2.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner3.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner4.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner5.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner6.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner1.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner2.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner3.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner4.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner5.png" alt="Image"></a></li>
                    <li><a href="#"><img src="images/partner6.png" alt="Image"></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
