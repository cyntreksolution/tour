<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mail\ContactMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * DashboardController constructor.
     */


    public function admin(Request $request)
    {
        $comments = Comment::NotReplyComment()->orderBy('status')->get();
        return view('admin.comments.index',compact('comments'));
    }

    public function debug(){
        $user=User::find(1);
//        Mail::to($user)->send(new ContactMail());
        return new ContactMail();
        return 1;
    }

    public function comments(Request $request){
        $comments = Comment::NotReplyComment()->orderBy('status')->get();
        return view('admin.comments.index',compact('comments'));
    }

    public function showComment(Request $request,Comment $comment){
        return view('admin.comments.show',compact('comment'));
    }

    public function approveComment(Request $request,Comment $comment){
        $comment->status=1;
        $comment->save();
        return redirect(route('all.comments'));
    }
    public function disapproveComment(Request $request,Comment $comment){
        $comment->status=0;
        $comment->save();
        return redirect(route('all.comments'));
    }

    public function reply(Request $request,Comment $comment){
        return view('admin.comments.reply',compact('comment'));
    }
    public function saveReply(Request $request,Comment $comment){
        $rply= new Comment();
        $rply->package_id = $comment->package->id;
        $rply->parent_id = $comment->id;
        $rply->name='Administrator';
        $rply->comment=$request->reply;
        $rply->status=1;
        $rply->email='admin';
        $rply->save();
        return redirect()->back();
    }

}
