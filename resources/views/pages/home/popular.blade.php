<section class="popular-packages">
    <div class="container">
        <div class="section-title text-center">
            <h2>Popular Packages</h2>
            <div class="section-icon">
                <i class="flaticon-diamond"></i>
            </div>
            <p>Find Travel Ideas For Planning Your Holiday To Sri Lanka.The Most Popular packages are ready on your request</p>
        </div>
        <div class="row package-slider slider-button">
            @foreach($popularPackages as $popularPackage)
                <div class="col-sm-4">
                    <div class="package-item">
                        <div class="package-image">
                            <img  src="images/packages/{{$popularPackage->package->image}}" alt="Image">
                            <div class="package-price">
                                <div class="deal-rating">
                                    @php
                                        $rating= $popularPackage->package->rating;
                                    @endphp
                                    @for($i=0;$i<5;$i++)
                                        @if($i<$rating)
                                            <span class="fa fa-star checked"></span>
                                        @else
                                            <span class="fa fa-star-o"></span>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="package-content">
                            <h3>{{$popularPackage->package->name}}</h3>
{{--                            <p class="package-days"><i--}}
{{--                                    class="flaticon-time"></i> {{$popularPackage->package->packageDescription->duration}}--}}
{{--                            </p>--}}
                            <p>{{$popularPackage->package->packageDescription->other}}</p>
                            <div class="package-info">
                                <a href="{{route('page.single',['package'=>$popularPackage->package_id])}}" class="btn-blue btn-red">View more details</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
<style>
    .popular-packages {
        padding-top: 100px !important;
    }

    .package-item img{
        width: 100%;
        height: 212px;
    }
</style>
