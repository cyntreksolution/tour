<?php
namespace App\Models;

class PackageImage extends \App\Models\AbstractModels\AbstractPackageImage
{
    public function getImageAttribute(){
       return $this->file_path.$this->file_name;
    }

    protected $appends=['image'];

   protected $fillable = ['package_id', 'filename'];

    public function package()
    {
        return $this->belongsTo('App\Package');
    }
}
