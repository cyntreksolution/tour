@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="dashboard-list-box">
                            <a href="{{route('timeline.form',['package'=>$package->id])}}" class="button preview">Add Timeline<i class="fa fa-arrow-circle-right"></i></a>
                            <h4 class="gray">{{$package->name}}</h4>
                            <div class="table-box">
                            <table class="basic-table booking-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Order</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package->packageTimelines as $key => $data)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$data->order}}</td>
                                        <td>{{$data->title}}</td>
                                        <td>{{$data->description}}</td>
                                        <td>
                                            <a href="{{route('package-timelines.edit',[$data->id])}}" class="button gray"><i class="sl sl-icon-pencil"></i></a>
                                            <form action="{{ route('package-timelines.destroy', $data->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"  type="submit"><i class="sl sl-icon-close"></i></button>
                                            <!-- <a href="#" class="button gray"><i class="sl sl-icon-close"></i></a> -->
                                            </form>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
@endsection
