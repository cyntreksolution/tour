@extends('layouts.default')

@section('content')
   @include('pages.home.banner')
   @include('pages.home.popular')
   @include('pages.home.deals')
   @include('pages.home.bucket')
   @include('pages.home.destinations')
   @include('pages.home.trip-ad')
   @include('pages.home.deals-on-sale')
{{--   @include('pages.home.countdown-section')--}}
   @include('pages.home.testimonials')
{{--   @include('pages.home.trusted-partners')--}}
@stop
