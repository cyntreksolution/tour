<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> Sri Lanka Tour Go </title>

    <meta name="description" content="The Best Travel Agent in Sri lanka to give most worth to your holiday in Sri Lanka"/>
    <meta name="keywords" content="places to visit in sri lanka ,sri lanka tourism,sri lanka tour,lonely planet sri lanka,srilanka tour package,sri lanka tourist places,sri lanka trip,sri lanka travel guide,travel agents in colombo" />

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/logo1.png')}}">
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('font/flaticon.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/plugin.css')}}" rel="stylesheet" type="text/css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-105029573-6"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-105029573-6');
    </script>


    <meta property="og:title" content="Sri Lanka Tour Go">
    <meta property="og:site_name" content="Sri Lanka Tour Go">
    <meta property="og:url" content="http://srilankatourgo.com/">
    <meta property="og:description" content="The Best Travel Agent in Sri Lanka to give most worth to your holiday in Sri Lanka">
    <meta property="og:type" content="business.business">
    <meta property="og:image" content="http://srilankatourgo.com/images/slider/slider1.jpeg">
    <meta property="business:contact_data:street_address" content="Gampaha">
    <meta property="business:contact_data:locality" content="Colombo">
    <meta property="business:contact_data:region" content="Western Province">
    <meta property="business:contact_data:postal_code" content="11000">
    <meta property="business:contact_data:country_name" content="Sri Lanka">

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '940245256310416');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=940245256310416&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>

<div id="preloader">
    <div id="status"></div>
</div>

<header>
    <div class="upper-head clearfix">
        <div class="container">
            <div class="contact-info contact-data">
                <p><i class="flaticon-phone-call"></i> Phone: <a
                        href="tel:{{$agent->telephone}}">{{$agent->telephone}}</a></p>
                <p><i class="flaticon-mail"></i> Mail: <a href="mailto:{{$agent->email}}">{{$agent->email}}</a></p>
            </div>
        </div>
    </div>
</header>

<!-- Navigation Bar -->
<div class="navigation">
    <div class="container">
        <div class="navigation-content">
            <div class="header_menu">
                <!-- start Navbar (Header) -->
                <nav class="navbar navbar-default navbar-sticky-function navbar-arrow">
                    <div class="logo pull-left">
                        <a href="/">
                            {{--                            <img alt="Image" src="images/Yatra-01.png">--}}
                            {{$agent->name}}
                        </a>
                    </div>
                    <div id="navbar" class="navbar-nav-wrapper pull-right">
                        <ul class="nav navbar-nav" id="responsive-menu">
                            <li class="{{ Request::is('/') ? 'active' : null }}"><a href="/">Home</a></li>
                            {{--                            <li class="{{ Request::is('categories') ? 'active' : null }}"><a href="/categories">Categories</a></li>--}}
                            <li class="{{ Request::is('all/package')? 'active' : null }}"><a href="/all/package">Packages</a>
                            </li>
                            <li class="{{ Request::is('about') ? 'active' : null }}"><a href="/about">About Us</a></li>
                            {{--                            <li class="{{ Request::is('contact') ? 'active' : null }}"><a href="/contact">Contact Us</a></li>--}}
                        </ul>
                    </div><!--/.nav-collapse -->
                    <div id="slicknav-mobile"></div>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- Navigation Bar Ends -->
@yield('content', 'Default Content')

<!-- Footer -->
<footer>
    <div class="footer-upper">
        <div class="container">
            <div class="footer-links">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="footer-about footer-margin">
                            <div class="about-logo">
                                <h3>{{$agent->name}}</h3>
                                {{--                                <img src="images/Yatra-white.png" alt="Image">--}}
                            </div>
                            <p>The Best Travel Agent in Sri lanka to give most worth to your holiday in Sri Lanka</p>
                            <div class="about-location">
                                <ul class="contact-data">
                                    <li><i class="flaticon-maps-and-flags" aria-hidden="true"></i> {{$agent->address}}
                                    </li>
                                    <li><i class="flaticon-phone-call"></i> {{$agent->telephone}}</li>
                                    <li><i class="flaticon-mail"></i> {{$agent->email}}</li>
                                </ul>
                            </div>
                            <div class="footer-social-links">
                                <ul>
                                    @if($agent->facebook)
                                        <li class="social-icon"><a target="_blank" href="{{$agent->facebook}}"><i class="fa fa-facebook"
                                                                                                  aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                    @if($agent->insta)
                                        <li class="social-icon"><a target="_blank" href="{{$agent->insta}}"><i class="fa fa-instagram"
                                                                                               aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                    @if($agent->twitter)
                                        <li class="social-icon"><a target="_blank" href="{{$agent->twitter}}"><i class="fa fa-twitter"
                                                                                                 aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                    @if($agent->youtube)
                                        <li class="social-icon"><a target="_blank" href="{{$agent->youtube}}">
                                                <i class="fa fa-youtube" aria-hidden="true"></i></a>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="footer-recent-post footer-margin">
                            <h3>Hot Deals</h3>
                            <ul>
                                @foreach($featuredPackages as $key => $featuredPackage)
                                    @if ($key<3)
                                        <li>
                                            <div class="recent-post-item">
                                                <div class="recent-post-image">
                                                    <img class="f-img"
                                                        src="{{asset('images/packages/'.$featuredPackage->package->image)}}"
                                                        alt="Image">
                                                </div>
                                                <div class="recent-post-content">
                                                    <h4>
                                                        <a href="{{route('page.single',['package'=>$featuredPackage->package_id])}}"> {{$featuredPackage->package->name}}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="footer-recent-post footer-margin">
                            <h3>Popular Trips</h3>
                            <ul>
                                @foreach($popularPackages as $key => $dealPackage)
                                    @if ($key<3)
                                        <li>
                                            <div class="recent-post-item">
                                                <div class="recent-post-image">
                                                    <img class="f-img"
                                                        src="{{asset('images/packages/'.$dealPackage->package->image)}}"
                                                        alt="Image">
                                                </div>
                                                <div class="recent-post-content">
                                                    <h4>
                                                        <a href="{{route('page.single',['package'=>$dealPackage->package_id])}}"> {{$dealPackage->package->name}}</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="footer-links-list">
                            <div class="footer-instagram">
                                <h3>Instagram</h3>
                                <ul>
                                    <li><img src="{{asset('images/insta1.jpg')}}" alt="Image"></li>
                                    <li><img src="{{asset('images/insta2.jpg')}}" alt="Image"></li>
                                    <li><img src="{{asset('images/insta3.jpg')}}" alt="Image"></li>
                                    <li><img src="{{asset('images/insta4.jpg')}}" alt="Image"></li>
                                    <li><img src="{{asset('images/insta5.jpg')}}" alt="Image"></li>
                                    <li><img src="{{asset('images/insta6.jpg')}}" alt="Image"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <div class="copyright-content">
                        <p>2018 <i class="fa fa-copyright" aria-hidden="true"></i> Powered By
                            <a href="#" target="_blank">Cyntrek Solutions</a></p>
                    </div>
                </div>
                {{--                <div class="col-xs-6">--}}
                {{--                    <div class="payment-content">--}}
                {{--                        <ul>--}}
                {{--                            <li>We Accept</li>--}}
                {{--                            <li>--}}
                {{--                                <img src="{{asset('images/payment1.png')}}" alt="Image">--}}
                {{--                            </li>--}}
                {{--                            <li>--}}
                {{--                                <img src="{{asset('images/payment2.png')}}" alt="Image">--}}
                {{--                            </li>--}}
                {{--                            <li>--}}
                {{--                                <img src="{{asset('images/payment3.png')}}" alt="Image">--}}
                {{--                            </li>--}}
                {{--                            <li>--}}
                {{--                                <img src="{{asset('images/payment4.png')}}" alt="Image">--}}
                {{--                            </li>--}}
                {{--                        </ul>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
            </div>
        </div>
    </div>
</footer>
<!-- Footer Ends -->

<!-- Back to top start -->
<div id="back-to-top">
    <a href="#"></a>
</div>
<!-- Back to top ends -->

<!-- *Scripts* -->
<script src="{{asset('https://kit.fontawesome.com/18602b848e.js')}}"></script>
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugin.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/main-1.js')}}"></script>
<script src="{{asset('js/custom-countdown.js')}}"></script>
<script src="{{asset('js/preloader.js')}}"></script>
</body>
<style>
    .contact-data a {
        color: white;
    }

    .f-img{
        /*width: 72px !important;*/
        height: 72px !important;
    }
</style>
</html>
