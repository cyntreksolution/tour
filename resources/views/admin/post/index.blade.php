@extends('layouts.backend.master')
@section('content')
<div class="dashboard-content">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="dashboard-list-box">
                            <h4 class="gray">All Comments</h4>
                            <div class="table-box">
                            <table class="basic-table booking-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>message</th>
                                        <th>status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($comments as $key => $comment)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>
                                         <li>
                                    <div class="comment-item">
                                        <div class="comment-image">
                                            <img src="images/author.jpg" alt="Image">
                                        </div>
                                        <div class="comment-content">
                                            <h4>{{$comment->name}}</h4>
                                            <p>{{$comment->comment}}</p>
                                            
                                        </div>
                                    </div>
                                     @foreach($comment->posts as $post)
                                     @if($comment->id === $post->comment_request_id)
                                    
                                    <ul class="reply">
                                        <li>
                                            <div class="comment-item">
                                                <div class="comment-image">
                                                    <img src="images/author.jpg" alt="Image">
                                                </div>
                                                <div class="comment-content">
                                                    <h4>Jack Richard</h4>
                                                    
                                                    <p>{{$post->reply}}</p>
                                                    
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    @endif 
                                @endforeach
                                    
                                    </li>
                                    </td>
                                        <td><a href="{{route('posts.edit',[$comment->id])}}" class="button gray"><i class="sl sl-icon-pencil"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
        

        $(".comment-container").delegate(".reply","click",function(){

            var well = $(this).parent().parent();
            var cid = $(this).attr("cid");
            var name = $(this).attr('name_a');
            var token = $(this).attr('token');
            var form = '<form method="post" action="/replies"><input type="hidden" name="_token" value="'+token+'"><input type="hidden" name="comment_id" value="'+ cid +'"><input type="hidden" name="name" value="'+name+'"><div class="form-group"><textarea class="form-control" name="reply" placeholder="Enter your reply" > </textarea> </div> <div class="form-group"> <input class="btn btn-primary" type="submit"> </div></form>';

            well.find(".reply-form").append(form);



        });

        $(".comment-container").delegate(".delete-comment","click",function(){

            var cdid = $(this).attr("comment-did");
            var token = $(this).attr("token");
            var well = $(this).parent().parent();
            $.ajax({
                    url : "/comments/"+cdid,
                    method : "POST",
                    data : {_method : "delete", _token: token},
                    success:function(response){
                    if (response == 1 || response == 2) {
                        well.hide();
                    }else{
                        alert('Oh ! you can delete only your comment');
                        console.log(response);
                    }
                }
            });

        });

        $(".comment-container").delegate(".reply-to-reply","click",function(){
            var well = $(this).parent().parent();
            var cid = $(this).attr("rid");
            var rname = $(this).attr("rname");
            var token = $(this).attr("token")
            var form = '<form method="post" action="/replies"><input type="hidden" name="_token" value="'+token+'"><input type="hidden" name="comment_id" value="'+ cid +'"><input type="hidden" name="name" value="'+rname+'"><div class="form-group"><textarea class="form-control" name="reply" placeholder="Enter your reply" > </textarea> </div> <div class="form-group"> <input class="btn btn-primary" type="submit"> </div></form>';

            well.find(".reply-to-reply-form").append(form);

        });


    }); 
            </script>
@endsection