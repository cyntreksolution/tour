@component('mail::message')
# Hi

Your have new booking request for  {{$bookTour->package->name }} . <br> <br>

Package:  {{$bookTour->package->name }}  <br>
Date: {{$bookTour->booking_date}} <br>
Name: {{$bookTour->name}} <br>
Email: {{$bookTour->email}} <br>
Telephone: {{$bookTour->telephone}} <br>
Message: {{$bookTour->message}} <br>

This is an automated email. Please do not reply to this email.<br>
Thanks,<br>
{{ preg_replace('/([a-z])([A-Z])/s','$1 $2', config('app.name')) }}
@endcomponent
